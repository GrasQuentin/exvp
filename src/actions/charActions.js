import { loadChars } from '../services/api';

export const LOAD_CHAR_REQUEST = 'load chars request';
export const LOAD_CHAR_SUCCESS = 'load chars success';
export const LOAD_CHAR_FAILURE = 'load chars failure';

export function loadChar() {
    return {
        type: LOAD_CHAR_REQUEST
    }
}

export function loadCharsSuccess(response) {
    return {
        type: LOAD_CHAR_SUCCESS,
        response
    }
}

export function loadCharsFailure(error) {
    return {
        type: LOAD_CHAR_FAILURE,
        error
    }
}


export function fetchHome() {
    return dispatch => {
        dispatch(loadChar());
        loadChars().then((result) => {
            dispatch(loadCharsSuccess(result));
        })
            .catch((error) => {
                dispatch(loadCharsFailure(error));
            });
    }
}