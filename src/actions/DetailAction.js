import { loadDetails } from '../services/api';

export const LOAD_DETAILS_REQUEST = 'load details request';
export const LOAD_DETAILS_SUCCESS = 'load details success';
export const LOAD_DETAILS_FAILURE = 'load details failure';

export function loadDetail() {
    return {
        type: LOAD_DETAILS_REQUEST
    }
}

export function loadDetailsSuccess(response) {
    return {
        type: LOAD_DETAILS_SUCCESS,
        response
    }
}

export function loadDetailsFailure(error) {
    return {
        type: LOAD_DETAILS_FAILURE,
        error
    }
}


export function fetchDetail(id) {
    return dispatch => {
        dispatch(loadDetail());
        loadDetails(id).then((result) => {
            dispatch(loadDetailsSuccess(result));
        })
            .catch((error) => {
                dispatch(loadDetailsFailure(error));
            });
    }
}