import { combineReducers } from 'redux'

import CharReducer from './CharReducer';
import DetailReducer from './DetailReducer'


export default combineReducers({
    home:CharReducer,
    details: DetailReducer
});


