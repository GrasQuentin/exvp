import * as actions from '../actions/charActions';

const initialState = [];

function reducer(state = initialState, action) {
    console.log(state, action);
    switch (action.type) {
        case actions.LOAD_CHAR_REQUEST:
            return { ...state, fetching_home:true };
        case actions.LOAD_CHAR_SUCCESS:
            return { ...state, fetching_home:false, response:action.response };
        case actions.LOAD_CHAR_FAILURE:
            return { ...state, fetching_home:false, error:action.error };
        default:
            return state;
    }
}

export default reducer;