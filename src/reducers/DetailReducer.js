import * as actions from '../actions/DetailAction';

const initialState = [];

function reducer(state = initialState, action) {
    console.log(state, action);
    switch (action.type) {
        case actions.LOAD_DETAILS_REQUEST:
            return { ...state, fetching_details:true };
        case actions.LOAD_DETAILS_SUCCESS:
            return { ...state, fetching_details:false, response:action.response };
        case actions.LOAD_DETAILS_FAILURE:
            return { ...state, fetching_details:false, error:action.error };
        default:
            return state;
    }
}

export default reducer;
