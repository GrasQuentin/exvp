import React, { Component } from 'react';
import { connect } from 'react-redux';
import {fetchDetail} from '../actions/DetailAction'
import EltDetails from '../Component/EltDetails'

class Details extends Component {
    constructor() {
        super(...arguments)
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchDetail(this.props.match.params.id));
    }

    render() {
        return (<div>
            {
                this.props.details.fetching_details ?
                    `Loading data for ${this.props.match.params.id} ...` :
                    <EltDetails data={this.props.details.response} />
            }
        </div>)
    }
}

export default connect(state => ({ details: state.details }))(Details);