import React, { Component } from 'react';
import {fetchHome} from '../actions/charActions'
import { connect } from 'react-redux';
import EltList from "../Component/EltList";

class Home extends Component {
    constructor() {
        super(...arguments)
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchHome())
    }



    render() {

        return (
            <div>
                {this.props.home.fetching_home ?
                    'Loading data ... ' :
                    this.props.home.error ?
                        <div>{ "error fetching server"}</div> :
                        <EltList data={this.props.home.response}/>
                }
            </div>
        )
    }
}

export default connect(state => ({ home: state.home }))(Home);