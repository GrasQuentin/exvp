import React from 'react';
import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom'

import Details from './container/Details'
import Home from './container/Home'

export default (
        <BrowserRouter>
            <div>
                <Route exact path="/" component={Home}/>
                <Route path="/details/:id" component={Details}/>
            </div>
        </BrowserRouter>

);