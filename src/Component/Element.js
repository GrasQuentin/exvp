import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Element extends Component {
    constructor() {
        super(...arguments)
    }

    render() {
        let result = this.props.element;
        let link = '/details/' + this.props.element.id;
        let links = result.urls.map((result) => {
            if (result.type === "detail")
                return (<div className="link">
                        <img className="book" src={require('../ressources/book.svg')} alt="img"/>
                        <Link className="link-text" to={link}>detail</Link>
                    </div>
                );
            return (<div className="link">
                <img className="book" src={require('../ressources/book.svg')} alt="img"/>
                <a className="link-text" href={result.url}>{result.type}</a>
            </div>);
        });
        return (
            <div className="element">
                <Link className="eltTop" to={link}>
                    <img className="photo-img" src={require('../ressources/noimage.jpg')} height="200" width="200" alt="img"/>
                    <div className="name">
                        {result.name}
                    </div>
                </Link>
                <div className="links">
                    {links}
                </div>

            </div>
        )
    }
}

