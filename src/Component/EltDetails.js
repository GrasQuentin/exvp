import React, { Component } from 'react';

export default class EltDetails extends Component {
    constructor() {
        super(...arguments)
    }

    render() {
        if (!this.props.data )
            return (<div className="EltList">"error</div>);
        let result = this.props.data.data.results[0];
        let series = result.series.items.map((elt) =>{
            return(
                <div>
                    <hr/>
                    <div className="listElt" >
                        {elt.name}
                    </div>
                </div>)
        });
        let comics = result.comics.items.map((elt) =>{
            return(
                <div>
                    <hr/>
                    <div className="listElt" >
                        {elt.name}
                    </div>
                </div>)
        });
        return(
            <div className="details">
                <img className="detail-img" src={require('../ressources/noimage.jpg')} height="150" width="150" alt="img"/>
                <div className="content">
                    <div className="details-header">
                        <h1>{result.name}</h1>
                        <div>{result.description}</div>
                    </div>
                    <div className="List">
                        <h3>Comics</h3>
                        {comics}
                    </div>
                    <div className="List">
                        <h3>Series</h3>
                        {series}
                    </div>
                </div>
            </div>
        )
    }
}

