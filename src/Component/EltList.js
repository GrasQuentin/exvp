import React, { Component } from 'react';
import Element from "./Element";

export default class EltList extends Component {
    constructor() {
        super(...arguments)
    }

    render() {
        let list;
        if (this.props.data) {
             list = this.props.data.data.results.map((result) => {
                return <Element element={result}/>
            });
         }
        else {
             list = "t"
        }
        return(<div className="eltList">{list}</div>)
    }
}

