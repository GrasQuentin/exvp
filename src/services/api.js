import {createHash,digest} from 'crypto'

const API_PUBLIC = '298bab46381a6daaaee19aa5c8cafea5';
const API_PRIVATE = 'b0223681fced28de0fe97e6b9cd091dd36a5b71d';
const BASE_URL = 'http://gateway.marvel.com:80';


export function loadChars() {
    return new Promise(function (resolve, reject) {
        const ts = Math.floor(Date.now() / 1000);
        const str = ts + API_PRIVATE + API_PUBLIC;
        const hash = createHash('md5').update(str).digest('hex');
        var url = BASE_URL + '/v1/public/characters?' + "ts=" + ts +  "&apikey=" + API_PUBLIC + "&hash=" + hash;
        fetch(url)
            .then((response) => {
                return (response.json());
            })
            .then(function (json) {
                resolve(json)
            })
            .catch((error) => {
                reject(error)

            });
    });
}

export function loadDetails(id) {
    return new Promise(function (resolve, reject) {
        const ts = Math.floor(Date.now() / 1000);
        const str = ts + API_PRIVATE + API_PUBLIC;
        const hash = createHash('md5').update(str).digest('hex');
        var url = BASE_URL + '/v1/public/characters/'+id+'?' + "ts=" + ts +  "&apikey=" + API_PUBLIC + "&hash=" + hash;
        fetch(url)
            .then((response) => {
                return (response.json());
            })
            .then(function (json) {
                resolve(json)
            })
            .catch((error) => {
                reject(error)

            });
    });
}

